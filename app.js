var express      = require('express')
var path         = require('path')
var favicon      = require('serve-favicon')
var logger       = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser   = require('body-parser')



var app       = express()
var server    = require('http').Server(app)
var expressWs = require('express-ws')(app, server)



var index      = require('./routes/index')
var api        = require('./routes/api')
var ws_polling = require('./routes/ws_polling')



app.locals.log = require("./log.js")
app.locals.log.reset()

app.locals.station = require("./station.js")
app.locals.station.load()

app.locals.d = require("./devices.js")
app.locals.d.load()

app.locals.s = require("./serial.js")
app.locals.s.reset()

app.locals.c = require("./controller.js")

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

var connect = require('./connect.js').poll()
var sync    = require('./sync.js').start()

var db  = require('./db.js').init()
var sql = require('./sql.js')
app.use(function(req,res,next){
    req.db  = db
    req.sql = sql
    next()
})

app.use('/',           index)
app.use('/api',        api)
app.use('/ws_polling', ws_polling)

app.use(function(req, res, next) {
    var err = new Error('Not Found')
    err.status = 404
    next(err)
})


app.use(function(err, req, res, next) {

    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    res.status(err.status || 500)
    res.render('error')

})

module.exports = { app: app, server: server }
