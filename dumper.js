// перенести в api
var fs       = require('fs')
var db       = require('./db.js')
var log      = require('./log.js')

exports.store = function(type, data) {

    var events_by_type = {
        weighing: {
            id:   10,
            name: 'weighing',
        },
        success_weighing: {
            id: 13,
            name: 'success_weighing',
        },
        station_error: {
            id: 1,
            name: 'station_error',
        },
    }

    var event = events_by_type[type]

    if ( event ) {

        db.db.none(`
            INSERT INTO
                events (type_id, value_int)
                VALUES ($1, $2)
        `,
        [ event.id, data ]
        )
        .then(() => {
            log.info( `event '${event.name}' inserted into database` )
        })
        .catch( error => {
            log.error( `event '${event.name}' inserting error: ${error}` )
        })

        if ( event.name == 'success_weighing' ) {
            db.db.none("UPDATE batches SET current_count = current_count-1 WHERE end_time IS NULL")
            .then(() => {
            })
            .catch( error => {
                log.error( `event '${event.name}', batch current_count update error: ${error}` )
            })
        }

    }
}