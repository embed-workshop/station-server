var serial  = require('./serial.js')
var devices = require('./devices.js')
var crc     = require('./crc.js')
var log     = require('./log.js')
var dumper  = require('./dumper.js')
var station = require('./station.js')
var helpers = require('./helpers.js')

exports._msg_type_command_send   = 0 // pi -> controller
exports._msg_type_command_ack    = 1 // controller -> pi
exports._msg_type_heartbeat_send = 2 // controller -> pi
exports._msg_type_heartbeat_ack  = 3 // pi -> controller
exports._msg_type_event_send     = 4 // controller -> pi
exports._msg_type_event_ack      = 5 // pi -> controller

exports.error_titles = [
    'все плохо', 'все очень плохо', 'убейте меня'
]

// обработка приходящих в serial сообщений
exports.process_msg = function(data) {

    switch (data[0]) {

        case exports._msg_type_heartbeat_send:
            var i = 1
            devices.list.forEach( (device, index) => {
                var state = device.state
                switch(state.type) {
                    case 'enum':
                        device.value = data[i++]
                        if ( state.values[device.value] ) {
                            device.display_value = state.values[device.value].title
                        }
                        else {
                            log.error( `Wrong value '${device.value}' for device '${device.title}'` )
                        }
                        break;
                    case 'uint':
                        device.value = helpers.buf_to_uint(data, i, state.size)
                        i+= state.size
                        device.display_value = device.value
                        break;
                }
            })
            break;

        case exports._msg_type_event_send:
            var device = devices.list[data[1]]
            var event  = device.events[data[2]]
            // fix if not exists
            var display_event = event.title + '- '
            var i = 3
            event.params.forEach(function(param, index) {
                display_event += param.title + ': '
                switch(param.type) {
                    case 'error_code':
                        display_event += exports.error_titles[data[i++]]
                        // добавить сохранение ошибок других устройств
                        break;
                    case 'uint':
                        var val = helpers.buf_to_uint(data, i, param.size)
                        i += param.size
                        // устройство 'Логика станции', ошибка
                        if ( device.code == 0 && event.code == 0 ) {
                            dumper.store( 'station_error', val )
                        }
                        // устройство 'Логика станции', успешное взвешивание
                        if ( device.code == 0 && event.code == 1 ) {
                            dumper.store( 'weighing', val )
                        }
                        // устройство 'Логика станции', успешное взвешивание с отбором
                        if ( device.code == 0 && event.code == 2 ) {
                            dumper.store( 'success_weighing', val )

                            if ( device.value == 3 ) {//взвешивание с отбором
                                var selection_count = station.params.selection_count || 0
                                var selection_total = station.params.selection_total
                                selection_count += 1
                                if ( selection_total == selection_count ) { // отобралось нужное количество
                                    selection_count = 0
                                    // переключить на 'обучение - открытый вход'
                                    exports.send_msg({
                                        device_code:  0,
                                        command_code: 1,
                                        params:       []
                                    })
                                }
                            }
                            station.write({
                                selection_count: selection_count
                            })
                        }
                        display_event += val.toString()
                        break;
                }
            })
            device.display_event = display_event
            break;
    }
}

exports.send_msg = function(data) {

    var msg = Buffer.alloc(20)

    msg[0] = exports._msg_type_command_send
    msg[1] = data.device_code
    msg[2] = data.command_code

    var i = 3

    devices.list[data.device_code].commands[data.command_code].params.map(
        (param) => {
            // add processing other params then uint
            // test helpers.uint_to_buf
            if (param.type == 'uint') {
                var size = param.size
                var val  = data.params.shift()
                for (var j = 0; j < size; j++) {
                    msg[i+size-1-j] = val & 0xFF
                    val = val >> 8;
                }
                i += size
            }
        }
    )

    crc.append(msg, 0, 18)

    serial.push(msg)

}