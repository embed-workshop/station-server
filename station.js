var jsonfile = require('jsonfile')
var fs       = require('fs')
var log      = require('./log.js')
var helpers  = require('./helpers.js')

exports.params = {}

exports.read = function(){
    var state_file = __dirname + '/state.json'
    var params = {}

    if (!fs.existsSync(state_file)) {
        var uid
        try {
            var content     = fs.readFileSync('/proc/cpuinfo', 'utf8');
            var cont_array  = content.split("\n");
            var serial_line = cont_array[cont_array.length-2];
            var serial = serial_line.split(":");
            uid = serial[1].slice(1);
        }
        catch(e) {
            uid = 'not_pi_' + helpers.get_random_int(0, 1000000)
        }

        jsonfile.writeFileSync(state_file, {
            uid:             uid,
            station_title:   'Default-' + uid,
        })

    }

    try {
        exports.params = jsonfile.readFileSync(state_file, {throws: true})
    }
    catch(e) {
        log.error('state file read error')
    }

    return exports.params
}

exports.write = function(params){
    for (var param in params) {
        exports.params[param] = params[param]
    }
    exports.store()
}

exports.store = function(){
    var state_file = __dirname + '/state.json'
    jsonfile.writeFileSync(state_file, exports.params)
}


exports.load = function() {

    exports.read()
    log.info('--- Station configuration loaded ---')

}