SELECT
    events.id AS id
    , event_types.id AS event_type_id
    , event_types.title
    , events.value_int
    , TO_CHAR(datetime, 'HH24:MI:SS') AS time
    , TO_CHAR(datetime, 'DD.MM.YYYY') AS date
    , events.value_text
FROM
    events LEFT JOIN event_types ON events.type_id = event_types.id
WHERE events.status = 1
  AND events.datetime > ${min_date}
  AND events.datetime < ${max_date}
ORDER BY
    events.datetime DESC
