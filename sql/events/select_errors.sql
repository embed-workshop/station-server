SELECT
    id
    , value_int
    , to_char(datetime, 'HH24:MI:SS') AS time
    , to_char(datetime, 'DD.MM.YYYY') AS date
FROM
    events
WHERE status  = 1
  AND type_id = 1
ORDER BY
    datetime DESC
