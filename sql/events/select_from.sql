SELECT
    id AS id
    , id AS local_id
    , type_id AS type_id
    , value_int AS value_int
    , value_text AS value_text
    , to_char(datetime, 'YYYY-MM-DD HH24:MI:SS.US') AS datetime
    , status AS status
FROM
    events
WHERE
    datetime > ${date_from}
