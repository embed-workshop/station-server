SELECT
    TO_CHAR(datetime, 'DD.MM.YYYY') AS date
    , TO_CHAR(datetime, 'YYYY-MM-DD') AS iso_date
    , MIN(   CASE WHEN type_id != 11 THEN value_int ELSE 1000 END) AS min_weight
    , MAX(   CASE WHEN type_id != 11 THEN value_int ELSE 0 END) AS max_weight
    , COUNT( CASE WHEN type_id != 11 THEN 1 END) AS total_weighings_count
    , COUNT( CASE WHEN type_id  = 13 THEN 1 END) AS success_weighings_count
    , COUNT( CASE WHEN type_id  = 11 THEN 1 END) AS death_count
    , SUM(   CASE WHEN type_id != 11 THEN value_int ELSE 0 END) AS total
FROM
    events
WHERE status = 1
  AND type_id IN (10, 11, 13)
  AND datetime > ${min_date}
  AND datetime < ${max_date}
GROUP BY
    date
    , iso_date
ORDER BY
    iso_date DESC
