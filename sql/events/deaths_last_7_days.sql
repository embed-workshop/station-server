SELECT
    COUNT (id) AS deaths_last_7_days
FROM
    events
WHERE
        type_id = ${type_id}
    AND datetime > ${min_date}
    AND datetime > NOW()-(interval '7 days')
