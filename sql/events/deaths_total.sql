SELECT
    COUNT (id) AS deaths_total
FROM
    events
WHERE
         type_id  = ${type_id}
    AND  datetime > ${min_date}
