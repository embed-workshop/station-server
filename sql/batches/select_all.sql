 SELECT 
    id
    , title
    , start_time
    , end_time
    , to_char(start_time, 'DD.MM.YYYY') AS start_date
    , to_char(end_time, 'DD.MM.YYYY') AS end_date
FROM
    batches
ORDER BY
    id DESC
