SELECT
    id AS id
    , id AS local_id
    , title AS title
    , to_char(start_time, 'YYYY-MM-DD HH24:MI:SS.US') AS start_time
    , to_char(end_time, 'YYYY-MM-DD HH24:MI:SS.US') AS end_time
    , start_count AS start_count
    , current_count AS current_count
    , status AS status
FROM
    batches
WHERE
    start_time >= ${date_from}
