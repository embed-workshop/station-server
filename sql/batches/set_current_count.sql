UPDATE
    batches
SET
    current_count = ${current_count}
WHERE
    end_time IS NULL
RETURNING *
