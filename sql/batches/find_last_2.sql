SELECT
    id
    , title
    , start_count
    , current_count
    , to_char(start_time, 'YYYY-MM-DD HH24:MI:SS.US') AS start_time
    , to_char(start_time, 'YYYY-MM-DD') AS start_date
    , (date('NOW()') - date(start_time)) AS days_from_start
FROM
    batches
ORDER BY
    id DESC
LIMIT 1
