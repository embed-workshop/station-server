UPDATE
    batches
SET
    current_count = current_count - 1
WHERE
    end_time IS NULL
RETURNING
    id
    , title
    , start_count
    , current_count
    , start_time
    , to_char(start_time, 'DD.MM.YYYY') AS start_date
    , (date('NOW()') - date(start_time)) AS days_from_start
