INSERT INTO
    batches (title, start_count, current_count)
VALUES
    (${title}, ${start_count}, ${current_count})
RETURNING
    id
    , title
    , start_count
    , current_count
    , (date('NOW()') - date(start_time)) AS days_from_start
    , to_char(start_time, 'DD.MM.YYYY') AS start_date
