UPDATE
    batches
SET
    end_time = NOW()
WHERE
    end_time IS NULL
