SELECT
    code AS command_code
    , code
    , title
    , value_int AS value
    , 0 AS device_code
    , 'значение' AS param
    , 'Задать' AS command_title
FROM
    settings
ORDER BY
    id ASC
