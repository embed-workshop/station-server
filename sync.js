const http   = require('http')

var log      = require('./log.js')
var config   = require('./config.js')
var station  = require('./station.js')

// url api сервера, в config.json
exports.main_server_url = config.read().main_server.url

// cpuid станции, в state.json
exports.cpuid           = station.params.uid

// период синхронизации, минут
exports.sync_period     = config.read().main_server.sync_period || 10

// начинаем синхронизацию через 5+cpuid%55 минут
// чтобы куча станций не синхронизировалась одновременно
exports.start = function() {

    setTimeout( function(){

        exports.poll()

    }, ( 5 + exports.cpuid%(exports.sync_period - 5) ) * 60 * 1000)

}

// запрос на синхронизацию
exports.poll = function() {

    // запрос sync
    http.get(`http://${exports.main_server_url}/sync/?cpuid=${exports.cpuid}`, (res) => {

        // запрос неуспешен
        //  - ждём период синхронизации и отправляем новый запрос
        if (res.statusCode !== 200) {
            log.error(`sync request failed`);
            setTimeout(exports.poll, exports.sync_period*60*1000)
            return;
        }

        var rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
            try {

                const parsedData = JSON.parse(rawData);
                // если запрос дошёл, но не был успешен, пишем в лог
                if (!parsedData.success) {
                    log.error(`sync request unsuccessful`);
                }

                //  ждём период синхронизации и отправляем новый запрос
                setTimeout( exports.poll, exports.sync_period*60*1000)

            } catch (e) {

                // пришедший json некорректен
                //  - ждём период синхронизации и отправляем новый запрос
                log.error(`sync request error: ${e.message}`);
                setTimeout(exports.poll, exports.sync_period60*1000)

            }
        });
    }).on('error', (e) => {

        // если запрос завершён ошибкой
        //  - ждём период синхронизации и отправляем новый запрос
        log.error(`sync request error: ${e.message}`);
        setTimeout(exports.poll, exports.sync_period*60*1000)

    });
}