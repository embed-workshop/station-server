var log      = require('./log.js')
var config   = require('./config.js')

exports.list = {}

exports.load = function() {

    exports.list = config.read().devices
    log.info('--- Devices configuration loaded ---')

}