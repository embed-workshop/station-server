var pgp      = require('pg-promise')(/*options*/)
var log      = require('./log.js')

exports.db

exports.init = function() {

    exports.db = pgp('postgres://pi:raspberry@localhost:5432/pi')
    log.info('--- Database loaded ---')
    return exports.db

}