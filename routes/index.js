var express = require('express')
var router  = express.Router()

var log        = require('../log.js')

const fs       = require('fs')

// refactor index
router.get('/', function(req, res, next) {

    res.render('index', {
        devices_list:   req.app.locals.d.list,
        station_params: req.app.locals.station.params
    })

})


router.get('/panel', function(req, res, next) {

    var render_params = {
        title:          'Станция - панель управления',
        devices_list:   req.app.locals.d.list,
        station_params: req.app.locals.station.params,
        batch:          {}
    }

    req.db.oneOrNone( req.sql.batches.find_last_2 )
    .then( batch => {

        if ( !batch ) {
            res.render( 'panel', render_params )
            return;
        }

        var query_params = {
            type_id:  11,//death
            min_date: batch.start_time
        }

        req.db.tx( t => {
            return t.batch([

                t.one(
                    req.sql.events.deaths_total,
                    query_params
                ),

                t.one(
                    req.sql.events.deaths_last_7_days,
                    query_params
                )

            ])
        })
        .then( data => {

            batch.deaths_total       = data[0].deaths_total
            batch.deaths_last_7_days = data[1].deaths_last_7_days
            render_params.batch      = batch
            res.render( 'panel', render_params )

        })
        .catch( error => {

            render_params.error_message = 'Ошибка. Перезагрузите страницу'
            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.render( 'panel', render_params )

        })        
    })
    .catch( error => {

        render_params.error_message = 'Ошибка. Перезагрузите страницу'
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.render( 'panel', render_params )

    })
})


router.get('/emulator', function(req, res, next) {

    res.render('emulator', {
        title:          'Станция - эмулятор',
        station_params: req.app.locals.station.params,
    })

})


router.get('/display', function(req, res, next) {

    res.render('display', {
    })

})


router.get('/display2', function(req, res, next) {

    res.render('display2', {
    })

})


router.get('/events', function(req, res, next) {

    var render_params = {
        title:          'Станция - события',
        data:           [],
        station_params: req.app.locals.station.params,
        batch:          {}
    }

    req.db.oneOrNone( req.sql.batches.find_last )
    .then( batch => {

        var min_date = batch.start_time,
            max_date = batch.end_time,
            query_params = {
                min_date: batch.start_time,
                max_date: batch.end_time || 'NOW()',
            }

        req.db.any(
            req.sql.events.select_by_date,
            query_params
        )
        .then( data => {

            render_params.data  = data
            render_params.batch = batch
            res.render( 'events', render_params )

        })
        .catch( error => {

            render_params.error_message = 'Ошибка. Перезагрузите страницу'
            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.render( 'events', render_params )

        })
    })
    .catch( error => {

        render_params.error_message = 'Ошибка. Перезагрузите страницу'
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.render( 'events', render_params )

    })
    
})


router.get('/errors', function(req, res, next) {

    var render_params = {
        title:          'Станция - ошибки',
        station_params: req.app.locals.station.params,
        data:           [],
    }

    req.db.any( req.sql.events.select_errors )
    .then( data => {

        render_params.data = data
        res.render( 'errors', render_params)

    })
    .catch( error => {

        render_params.error_message = 'Ошибка. Перезагрузите страницу'
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.render( 'errors', render_params )

    })

})


router.get('/settings', function(req, res, next) {

    var render_params = {
        title:          'Станция - настройки',
        station_params: req.app.locals.station.params,
        data:           [],
    }

    req.db.any( req.sql.settings.select_all )
    .then( data => {

        data.push({
            device_code:   1,
            command_code:  0,
            command_title: 'Калибровать нулевое значение',
            value:         '',
        })
        data.push({
            device_code:   1,
            command_code:  1,
            command_title: 'Рассчитать gain',
            value:         '',
            param:         'reference_weight'
        })

        render_params.data = data
        res.render( 'settings', render_params )

    })
    .catch( error => {

        render_params.error_message = 'Ошибка. Перезагрузите страницу'
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.render( 'settings', render_params )

    })

})


router.get('/statistics', function(req, res, next) {

    var render_params = {
        station_params:     req.app.locals.station.params,
        data:              [],
        batches:           [],
        selected_batch_id: undefined,
        title:             'Станция - статистика'
    }

    req.db.any( req.sql.batches.select_all )
    .then( batches => {

        var batch_id = req.query.batch_id
        if ( batches.length ) {
            batch_id = batch_id || batches[0].id
        }

        var min_date,
            max_date
        batches.forEach( batch => {
            if (batch.id == batch_id) {
                min_date = batch.start_time
                max_date = batch.end_time
            }
        })

        var query_params = {
            min_date: min_date,
            max_date: max_date || 'NOW()',
        }

        req.db.any( req.sql.events.stats, query_params )
        .then( data => {

            render_params.data              = data
            render_params.batches           = batches
            render_params.selected_batch_id = batch_id
            res.render('statistics', render_params)

        })
        .catch( error => {

            render_params.error_message = 'Ошибка. Перезагрузите страницу'
            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.render( 'settings', render_params )

        })
    })
    .catch( error => {

        render_params.error_message = 'Ошибка. Перезагрузите страницу'
        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.render( 'settings', render_params )

    })
 
})


module.exports = router
