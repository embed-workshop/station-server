var express = require('express')
var router  = express.Router()

var log     = require('../log.js')


router.get('/emulate', function(req, res, next) {
    req.app.locals.c.process_msg(Buffer.from(req.query.bytes.map((byte) => byte & 0xFF)))
    res.json({success: 1})
})


router.get('/command', function(req, res, next) {

    var command_code = parseInt( req.query.command_code )
    var device_code  = parseInt( req.query.device_code )
    var params       = (req.query.params || []).map( param => parseInt( param ) )

    log.info('--- Got command from panel ---')
    log.info(
        'device_code: %d, command_code: %d' + (params.length ? ', params: ' + params.map(param => '%s').join(', ') : ''),
        device_code,
        command_code,
        ...params
    )

    // задать режим взвешивания с отбором
    if ( device_code == 0 && command_code == 3 ) {
        req.app.locals.station.write({
            selection_total: params[0],
            min_weight:      params[1],
            max_weight:      params[2]
        })
        params.shift()//remove selection_total
    }

    var param_value = params[0]
    req.app.locals.c.send_msg({
        device_code:  device_code,
        command_code: command_code,
        params:       params
    })

    // задание настроек
    if (   device_code == 0
        && 4 <= command_code && command_code <= 11) {

        req.db.none(
            req.sql.settings.update,
            { value_int: param_value, code: command_code }
        )
        .then(() => {

            res.json({
                success: 1,
                setting: command_code,
                value:   param_value
            })

        })
        .catch( error => {

            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.json({ success: 0 })

        })

    }
    else {
        res.json({ success: 1, value: req.app.locals.station.params })
    }

})


router.get('/state', function(req, res, next) {

    res.json( req.app.locals.d.list.map( device => { return {
        display_value: device.display_value,
        display_event: device.display_event
    }}))

    req.app.locals.d.list.forEach( device => {
        device.display_event = ''
    })

})


router.post('/create_batch', function(req, res, next) {

    var query_params = {
        title:         req.body.title,
        start_count:   req.body.start_count,
        current_count: req.body.start_count,
    }

    req.db.none( req.sql.batches.end_current )
    .then(() => {
        req.db.one(
            req.sql.batches.create,
            query_params
        )
        .then( batch => {

            res.json({ success: 1, value: batch })

        })
        .catch( error => {

            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.json({ success: 0 })

        })
    })
    .catch( error => {

        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.json({ success: 0 })

    })
})


router.post('/fix_current_count', function(req, res, next) {

    req.db.tx( t => {
        return t.batch([
            t.one(
                req.sql.batches.set_current_count,
                { current_count: req.body.current_count }
            ),
            t.one(
                req.sql.events.create,
                {
                    type_id:    12, //correction
                    value_int:  req.body.current_count,
                    value_text: req.body.title
                }
            )
        ])
    })
    .then( data => {

        res.json({ success: 1, value: data[0] })

    })
    .catch( error => {

        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.json({ success: 0 })

    })

})


router.post('/pig_death', function(req, res, next) {

    req.db.none(
            req.sql.events.create,
            {
                type_id:    11,
                value_int:  1,
                value_text: req.body.title
            }
    )
    .then(() => {
        req.db.oneOrNone( req.sql.batches.dec_current_count )
        .then( batch => {

            if ( !batch ) {
                res.json({ success: 0 })
                return;
            }

            var query_params = {
                type_id:  11,//death
                min_date: batch.start_time
            }
            req.db.tx( t => {
                return t.batch([

                    t.one(
                        req.sql.events.deaths_total,
                        query_params
                    ),

                    t.one(
                        req.sql.events.deaths_last_7_days,
                        query_params
                    )

                ])
            })
            .then( data => {

                batch.deaths_total       = data[0].deaths_total
                batch.deaths_last_7_days = data[1].deaths_last_7_days
                res.json({ success: 1, value: batch })

            })
            .catch( error => {

                log.error(`route ${req.originalUrl}, db query error: ${error}`)
                res.json({ success: 0 })

            })        
        })
        .catch( error => {

            log.error(`route ${req.originalUrl}, db query error: ${error}`)
            res.json({ success: 0 })

        })
    })
    .catch( error => {

        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.json({ success: 0 })

    })
})


router.post('/station_state', function(req, res, next) {

    req.app.locals.station.write( req.body )

    if ( req.body.min_weight || req.body.max_weight ) {
        // изменить параметры взвешивания с отбором
        req.app.locals.c.send_msg({
            device_code:  0,
            command_code: 3,
            params: [
                req.app.locals.station.params.min_weight,
                req.app.locals.station.params.max_weight
            ]
        })
    }

    res.json({ success: 1, value: req.app.locals.station.params })
})


router.get('/station_state/:title', function(req, res, next) {

    var old_title = req.app.locals.station.params.station_title,
        new_title = req.params.title

    req.app.locals.station.write({ station_title: new_title })

    res.json({
        success:   1,
        new_title: new_title,
        old_title: old_title
    })

})


router.post('/remove_event', function(req, res, next) {

    req.db.none( req.sql.events.delete, { id: req.body.id } )
    .then(() => {

        res.json({ success: 1 })

    })
    .catch( error => {

        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.json({ success: 0 })

    })

})


router.post('/undo_event', function(req, res, next) {

    var event_id = req.body.id

    req.db.tx( t => {

        return t.batch([
            t.one( req.sql.batches.find_current ),
            t.one( req.sql.events.find, { id: event_id } )
        ])

    })
    .then( data => {

        var batch = data[0],
            event = data[1]

        // 13 - success weighing - успешное взвешивание, 11 - death - смерть
        if ( event.type_id == 13 || event.type_id == 11 ) {

            if ( event.type_id == 13 ) {
                req.app.locals.station.write({
                    selection_count: req.app.locals.station.params.selection_count-1
                })
            }

            req.db.tx( t => {
                return t.batch([

                    t.none( req.sql.events.delete,             { id: event.id } ),
                    t.none( req.sql.batches.inc_current_count, { id: batch.id } ),

                ])
            })
            .then(() => {

               res.json({ success: 1 })

            })
            .catch( error => {

                log.error(`route ${req.originalUrl}, db query error: ${error}`)
                res.json({success: 0 })

            })
        }

        // 10 - weghing - взвешивание
        else if ( event.type_id == 10 ) {
            req.db.tx( t => {
                return t.batch([

                    t.none( req.sql.events.delete,             { id: event.id } ),

                ])
            })
            .then(() => {

               res.json({ success: 1 })

            })
            .catch( error => {

                log.error(`route ${req.originalUrl}, db query error: ${error}`)
                res.json({success: 0 })

            })
        }

        // TODO - добавить отмену других событий
        else {
            res.json({success: 0 })
        }
    })
    .catch( error => {

        log.error(`route ${req.originalUrl}, db query error: ${error}`)
        res.json({success: 0 })

    })
})

router.get('/data', function(req, res, next) {

    var type = req.query.type
    var date_from = req.query.date_from

    switch (type) {
        case 'batch':
            req.db.manyOrNone(
                req.sql.batches.select_from,
                { date_from: date_from }
            )
            .then( data => {

                res.json(data)

            })
            .catch( error => {

                log.error(`route ${req.originalUrl}, db query error: ${error}`)
                res.json({ success: 0 })

            })
            break;

        case 'event':
            req.db.manyOrNone(
                req.sql.events.select_from,
                { date_from: date_from }
            )
            .then( data => {

                res.json(data)

            })
            .catch( error => {

                log.error(`route ${req.originalUrl}, db query error: ${error}`)
                res.json({ success: 0 })

            })
            break;

        default:
            res.json({ success: 0 })
            break;
    }   

})


module.exports = router
