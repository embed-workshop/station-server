var express = require('express')
var router  = express.Router()


router.ws('/weighing', function(ws, req) {
    ws.on('message', function(msg) {
        try {
            ws.send( JSON.stringify({
                mode:            req.app.locals.d.list[0].value,
                value:           req.app.locals.d.list[1].display_value,
                min_weight:      req.app.locals.station.params.min_weight,
                max_weight:      req.app.locals.station.params.max_weight,
                selection_count: req.app.locals.station.params.selection_count,
                selection_total: req.app.locals.station.params.selection_total
            }))
        }
        catch(e) {
            req.app.locals.log.error('--- Websocket poll error - weighing')
        }
    })
})


router.ws('/states', function(ws, req) {
    ws.on('message', function(msg) {
        try {
            var states = req.app.locals.d.list.map( device => device.value )
            states.push( req.app.locals.station.params.selection_count )
            ws.send( JSON.stringify( states ))
        }
        catch(e) {
            req.app.locals.log.error('--- Websocket poll error - states')
        }
    })
})


module.exports = router