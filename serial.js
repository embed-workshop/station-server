var SerialPort = require('serialport')
var crc        = require('./crc.js')
var log        = require('./log.js')
var c          = require('./controller.js')
var config     = require('./config.js')

exports.debug = config.read().serialport.debug

exports.error = ''

exports.port

const msg_size = 20

exports.parser = function(length) {
    var data = new Buffer(0)
    var synced = 0
    return function(emitter, buffer) {
        data = Buffer.concat([data, buffer])        
        while (data.length >= length) {
            if (synced) {
                if (crc.check(data, 0, length)) {
                    emitter.emit('data', data.slice(0, length))
                    data = data.slice(length)
                }
                else {
                    synced = 0
                    data = data.slice(length)
                }
            }
            else {
                if (crc.check(data, 0, length)) {
                    emitter.emit('data', data.slice(0, length))
                    data = data.slice(length)
                    synced = 1
                }
                else {
                    data = data.slice(1)
                }
            }
        }
    }
}

function open_port(params) {

    exports.port = new SerialPort(params.port_name, {
        autoOpen: false,
        baudRate: params.baud_rate,
        parser:   exports.parser(msg_size),
        // parser:   SerialPort.parsers.byteLength(msg_size)
    })

    exports.port.on('open', function() {
        log.info( '--- Serial Port opened ---')
        exports.error = ''

    })

    exports.port.on('error', function(data) {
        // log port error
        exports.error = 'uart error'
    })

    exports.port.on('data', function(data) {

        exports.error = ''
        if (exports.debug) {
            log.debug('Message <- ')
            log.debug(data)
        }
        try {
            c.process_msg(data)
        }
        catch (e) {
            log.error(`process_msg error ${e} on message:`)
            log.error( data )
        }
    })

    exports.port.open(function(err){
        if (err) {
            return
        }
    })
}

exports.reset = function() {

    var params = config.read().serialport
    if (params) {

        if (exports.port) {
            exports.port.close(function() {
                open_port(params)
            })
        }
        else {
            open_port(params)
        }

    }
}

exports.push = function(query) {

    exports.get(query)

}

exports.get = function(query) {
    if (!exports.error) {

        if (exports.debug) {
            log.debug('Message -> ')
            log.debug(query)
        }
        exports.port.write(query, function () {
            exports.port.drain(function(){ 

            })
        })

    }
}
