$(document).on('click', '.remove', function(){
    var tr = $(this).parent().parent()
    $.post('/api/remove_event', {
        id: $(this).data('id')
    })
    .done(function( data ) {
        if (data.success) {
            tr.remove()
        }
    })
    .fail(function() {

    })
})

$(document).on('click', '.undo', function(){
    var tr = $(this).parent().parent()
    $.post('/api/undo_event', {
        id: $(this).data('id')
    })
    .done(function( data ) {
        if (data.success) {
            tr.remove()
        }
    })
    .fail(function() {

    })
})