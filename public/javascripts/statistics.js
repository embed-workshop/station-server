$(document).on('change', 'select', function(e) {
    document.location.href = '/statistics?batch_id=' + $(this).val()
})

$(document).on('click', '#save_as_xlsx', function(e) {
    var formatted_data = data.map((datum) => [
        datum.date,
        datum.total_weighings_count,
        Math.round(datum.total/datum.total_weighings_count),
        '',
        datum.min_weight,
        datum.max_weight,
        datum.success_weighings_count,
        datum.death_count,
        ''
    ])
    formatted_data.unshift([
        'Дата',
        'Кол-во взвешиваний',
        'Средняя масса',
        'Средний привес',
        'Мин. масса',
        'Макс. масса',
        'Кол-во избранных',
        'Кол-во павших',
        'Кол-во в партии'
    ])
    saveXLSX(formatted_data, 'Статистика')
})


//----------------------------------------------------------------------------
// HIGHCHARTS
var plot_data = {
    "weekdays_tooltips":
        [],
    "x_axis_categories":
        [],
    "tooltips":
        [],
    "total_tooltips":
        [],
    "average_weight":
        [],
    "average_gain":
        [],
    "trend_line":
        [],
};

data.forEach(function(datum) {
    plot_data.weekdays_tooltips.push(datum.date)
    plot_data.x_axis_categories.push(datum.date)
    plot_data.tooltips.push(datum.date)
    plot_data.total_tooltips.push(datum.date)
    plot_data.average_weight.push(Math.round(datum.total/datum.total_weighings_count))
})

function format_x_label() {
    return this.value;
}

function format_number(value) {
    if (value == null) {
        return 'Нет данных';
    }
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function format_y_label() {
    if (this.value < 1000) 
        return this.value;
    else if (this.value < 1000000)
        return '' + this.value / 1000 + ' тыс';
    else if (this.value < 1000000000)
        return '' + this.value / 1000000 + ' млн';
    else
        return '' + this.value / 1000000000 + ' млрд';
}

function format_weekend_tooltip() {

    var tooltip_date = 
          this.series.name == 'Средняя масса' ? plot_data.tooltips[this.x]
        // : this.series.name == 'БУДНИ'  ? plot_data.weekdays_tooltips[this.x]
        : '';

    // if (this.x == 0 && this.series.name == 'БУДНИ') {
    //     return 'ПРЕВЬЮ:'
    //         + format_number(this.y);
    // }

    return tooltip_date
        + '<br>'
        + this.series.name + ': '
        + format_number(this.y);
}

function format_total_tooltip() {
    if (this.x == 0) {
        return '';
    }
    return (this.x).toString() + ' неделя'
        + '<br>'
        +  plot_data.total_tooltips[this.x-1] 
        + '<br>'
        + 'ВСЕГО: '
        + format_number(this.y);
}

$(function () {
    if (plot_data.x_axis_categories) {
        $('#weighing_count').highcharts({

            colors: ['#807e7c', '#ef7a7a', '#241600'],

            chart: {
                backgroundColor : '#ffffff',
                width:700,
                height:400,
                spacingLeft:22,
                spacingRight:22,
                spacingBottom:15,
            },

            title: {
                text: null,
                floating: true,
            },

            xAxis: {
                categories: plot_data.x_axis_categories,
                tickmarkPlacement: 'on',
                labels: {
                    align:    'left',
                    rotation: 0,
                    style     : {
                        color : '#111111',
                        fontSize: '9.5pt',
                    },
                    y: 25,
                    x: 15,      
                    formatter : format_x_label,
            
                },
                lineWidth: 0,
                lineColor: '#6a6a6a',
                tickLength: 0,
                tickWidth: 1,
                tickColor: '#FFFFFF',
                gridLineWidth: 0,
                // max: 5,
                showLastLabel: true,
                plotBands:[
                    {
                        color: '#ffffff',
                        from: 0,
                        to: 1,
                    },
                    {
                        color: '#ffffff',
                        from: 1,
                        to: 2,
                    },
                    {
                        color: '#ffffff',
                        from: 2,
                        to: 3,
                    },
                    {
                        color: '#ffffff',
                        from: 3,
                        to: 4,
                    },
                    {
                        color: '#ffffff',
                        from: 4,
                        to: 5,
                    },
                    {
                        color: '#ffffff',
                        from: 5,
                        to: 6,
                    }
                ]
            },

            yAxis: [
                {
                    lineWidth: 0,
                    lineColor: '#6a6a6a',
                    tickLength: 0,
                    tickWidth: 1,
                    tickColor: '#FFFFFF',
                    labels: {
                        formatter : format_y_label,
                        style     : {
                            color : '#111111',
                            fontSize: '9.5pt',
                            },
                        },
                    gridLineDashStyle: 'Solid',
                    gridLineColor: '#e7e7e8',
                    min: 0,
                    title: {
                        text: 'Средняя масса',
                        align: 'high',
                        rotation: 0,
                        offset: 0,
                        y: -24,
                        x: -15,
                        style: {
                            color : '#111111',
                            fontSize: '11pt',
                            fontWeight: '700',
                        }
                    },
                },
                // {
                //     lineWidth: 0,
                //     lineColor: '#6a6a6a',
                //     tickLength: 0,
                //     tickWidth: 1,
                //     tickColor: '#e9bd0f',
                //     labels: {
                //         formatter : format_y_label,
                //         style     : {
                //             color : '#ef7a7a',
                //             fontSize: '9.5pt',
                //         },
                //     },
                //     gridLineDashStyle: 'Solid',
                //     gridLineColor: '#e7e7e8',
                //     min: 0,
                //     title: {
                //         text: 'Средний привес',
                //         align: 'high',
                //         rotation: 0,
                //         offset: 0,
                //         y: -24,
                //         x: 58,
                //         style: {
                //             color : '#ef7a7a',
                //             fontSize: '11pt',
                //             fontWeight: '700',
                //         },
                //     },
                //     opposite: true,
                // },
            ],

            series: [
                {
                    type: 'column',
                    name: 'Средняя масса',
                    yAxis: 0,
                    data: plot_data.average_weight,
                },
                // {
                //     type: 'column',
                //     name: 'Средний привес',
                //     yAxis: 0,
                //     data: plot_data.average_gain,
                // },
                // {
                //     type: 'spline',
                //     name: 'Тренд',
                //     yAxis: 1,
                //     data: plot_data.trend_line,
                // },
            ],

            plotOptions: {
                column: {
                    groupPadding: 0.1,
                    pointPadding: 0.1,
                    pointPlacement: 'between',
                    borderWidth: 0,
                    events: {
                        legendItemClick: function () {return false;}
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormatter: format_weekend_tooltip,
                    }
                },
                spline: {
                    dashStyle: "Dash",
                    lineWidth: 1,
                    pointPlacement: 'on',
                    marker: {
                        enabled: true,
                    },
                    events: {
                        legendItemClick: function () {return false;}
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormatter: format_total_tooltip,
                    },
                    states: {
                        hover: {
                            enabled: true,
                            lineWidth: 1,
                            lineWidthPlus: 0,
                            halo: {
                                attributes: {
                                  fill: "#ef7a7a"
                                },
                            },

                        },
                    },
                },
            },

            legend: {
                verticalAlign: 'top',
                y: 5,
                x: 0,
                itemStyle: { 
                    "color": "#111111", 
                    "fontSize": "9.5pt", 
                    "fontWeight": "normal", 
                    },
                itemHoverStyle: { 
                    "color": "#111111", 
                    "fontSize": "9.5pt", 
                    "fontWeight": "normal", 
                    },                               
                },


            credits: {
                enabled: false,
                },

            tooltip: {
                backgroundColor: "#ef7a7a",
                color: "#ffffff",
                borderWidth: 0,
                shadow: true,
                style: {
                    color: "#ffffff",
                    fontSize: "9.5pt",
                    },
            }

        });
    }
});


//----------------------------------------------------------------------------
// XLSX
function saveXLSX(data, ws_name){

    var wb = new Workbook(), ws = sheet_from_array_of_arrays(data);

    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;
    var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), "statistics.xlsx")
}

function Workbook() {
        if(!(this instanceof Workbook)) return new Workbook();
        this.SheetNames = [];
        this.Sheets = {};
    }

function datenum(v, date1904) {
    if(date1904) v+=1462;
    var epoch = Date.parse(v);
    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}
function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function sheet_from_array_of_arrays(data, opts) {
    var ws = {};
    var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
    for(var R = 0; R != data.length; ++R) {
        for(var C = 0; C != data[R].length; ++C) {
            if(range.s.r > R) range.s.r = R;
            if(range.s.c > C) range.s.c = C;
            if(range.e.r < R) range.e.r = R;
            if(range.e.c < C) range.e.c = C;
            var cell = {v: data[R][C] };
            if(cell.v == null) continue;
            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

            if(typeof cell.v === 'number') cell.t = 'n';
            else if(typeof cell.v === 'boolean') cell.t = 'b';
            else if(cell.v instanceof Date) {
                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                cell.v = datenum(cell.v);
            }
            else cell.t = 's';

            ws[cell_ref] = cell;
        }
    }
    if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
    return ws;
}
