var ws_interval,
    ws,
    mode = 0

var poll = function() {

    if (ws_interval) {
        clearInterval(ws_interval)
    }

    ws = new WebSocket('ws://' + window.location.host + '/ws_polling/weighing')

    ws.onopen = function() {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        ws_interval = setInterval(function(){
            try {
                ws.send('ping')
            }
            catch(e) {
            }
        }, 200)
    };

    ws.onclose = function(event) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        setTimeout( poll, 1000);
    };

    ws.onmessage = function(event) {

        var data = JSON.parse(event.data)
        if ( data.mode != mode ) {
            mode = data.mode
            $('.info ul').hide()
            $('.info ul:nth-child('+(mode+1)+')').show()
        }

        if ( data.mode != 3 ) {
            data.min_weight = '0'
            data.max_weight = '200'
        }
        else {
            var weights = 'от ' + data.min_weight + ' до ' + data.max_weight
            if ($('#weights').text() != weights) {
                $('#weights').html(weights)
            }
            var selection = (data.selection_count || '0') + ' из ' + data.selection_total
            if ($('#selection').text() != selection) {
                $('#selection').html(selection)
            }
        }

        if ( gauge.value() != data.value ) {
            gauge.value(data.value)
        }

        if (   gauge.option('rangeContainer.ranges[1].startValue') != data.min_weight
            || gauge.option('rangeContainer.ranges[1].endValue')   != data.max_weight) {

            gauge.beginUpdate()
            gauge.option('rangeContainer.ranges[0].endValue',   data.min_weight)
            gauge.option('rangeContainer.ranges[1].startValue', data.min_weight)
            gauge.option('rangeContainer.ranges[1].endValue',   data.max_weight)
            gauge.option('rangeContainer.ranges[2].startValue', data.max_weight)
            gauge.option('rangeContainer.ranges[2].endValue',   parseInt(data.max_weight) + parseInt(data.min_weight))
            gauge.option('scale.endValue',                      parseInt(data.max_weight) + parseInt(data.min_weight))
            gauge.endUpdate()
        }

        if ( $('.value').html() != data.value ) {
            $('.value').html(data.value)
        }

    };

    ws.onerror = function(error) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
    };

}

var gauge
$(document).ready(function(){

    gauge = $('#gaugeContainer').dxCircularGauge({
        // animation: {
        //     enabled: false
        // },
        scale: {
            startValue: 0,
            endValue: 150,
            tickInterval: 5,
            label: {
                font: {
                    size: 20,
                    style: 'bold',
                    color: '#000000'
                }
            }
        },
        rangeContainer: {
            ranges: [
                {
                    startValue: 0,
                    endValue: 70,
                    color: '#FF0000'
                },
                {
                    startValue: 70,
                    endValue: 75,
                    color: '#00FF00'
                },
                {
                    startValue: 75,
                    endValue: 150,
                    color: '#FF0000'
                }
            ],
            width: 40,
            orientation: 'center',
        },
        value: 75,
        valueIndicator: {
            type: "triangleNeedle",
            color: "#000000",
            width: 20

        },
    }).dxCircularGauge('instance');

    poll()

})

