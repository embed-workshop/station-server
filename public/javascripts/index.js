$(document).on('click', '.commands li a', function(e) {

    e.preventDefault()

    var params = []
    $(this).siblings('.command_param').each(function (index, element) {
        params.push($(element).val())
    })

    $.get( '/api/command', {
        device_code:  $(this).data('device_code'),
        command_code: $(this).data('command_code'),
        params:       params
    })
    .done(function( data ) {

    })
    .fail(function() {

    })
});

$(document).on('click', '.events li a', function(e) {

    e.preventDefault()

    $(this).parent().remove()
});

var activity_flag = true

$(document).ready(function() {
    setInterval(function(){
        if ($('#is_rt_cbx').is(':checked')) {
            $.get('/api/state', {

            })
            .done(function( data ) {
                $.each(data, function(index, value) {
                    $('ul.devices>li:nth-child('+(index+1)+')>input').val(value.display_value)
                    if (value.display_event) {
                        $('ul.devices>li:nth-child('+(index+1)+') ul.events').append('<li><span>' + value.display_event + '</span><a href="" class="remove_event">удалить</a></li>')
                    }
                })
            })
            .fail(function() {

            })
        }
    }, 200)

    // $('body').mousemove(function(){
    //     if (!activity_flag) {
    //         activity_flag = true
    //         $('#is_rt_cbx').prop('checked', true)
    //     }
    // })

    // setInterval(function(){
    //     if (!activity_flag) {
    //         $('#is_rt_cbx').prop('checked', false)
    //     }
    //     activity_flag = false
    // }, 1*60*1000)

})
