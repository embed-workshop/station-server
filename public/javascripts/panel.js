var ws_interval
var ws

var poll = function() {

    if (ws_interval) {
        clearInterval(ws_interval)
    }

    ws = new WebSocket('ws://' + window.location.host + '/ws_polling/states')

    ws.onopen = function() {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        ws_interval = setInterval(function(){
            try {
                ws.send('ping')
            }
            catch(e) {
                setTimeout( poll, 1000 );
            }
        }, 200)
    };

    ws.onclose = function(event) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        setTimeout( poll, 1000);
    };

    ws.onmessage = function(event) {
        $.each(JSON.parse(event.data), function(index, value) {
            value += 0
            if (index == 0) { // устройство 'логика станции'
                $('#station_mode table').hide()
                $('#station_mode table:nth-child(' + (value+1) + ')').show()


                $('#station_mode_buttons li').hide()
                $('#station_mode_buttons li.mode_' + value).show()
            }
            // HACK
            else if (index == 10) {
                $('#selection_count').html(value)
            }
            else {
                if (devices_list[index].state.type == 'enum') {
                    // if (value === '') {
                    // } else {
                        value = devices_list[index].state.values[value].title
                    // }
                }
                if ($('#station_state tr:nth-child(' + index + ') td.display_value').html() != value) {
                    $('#station_state tr:nth-child(' + index + ') td.display_value').html(value)
                }
            }
        })
    };

    ws.onerror = function(error) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        // setTimeout( poll, 1000);
    };


}
$(document).ready(function(){

    poll()

})

$(document).on('click', 'a.command_button', function(e) {

    e.preventDefault()

    var params = []
    $(this).siblings('.command_param').each(function (index, element) {
        params.push($(element).val())
    })

    $.get( '/api/command', {
        device_code:  $(this).data('device_code'),
        command_code: $(this).data('command_code'),
        params:       params
    })
    .done(function( data ) {

    })
    .fail(function() {

    })
});

$(document).on('click', '.toggle_station_mode a', function(e) {

    $('.toggle_station_mode input').removeClass('error')
    var has_error = 0
    $(this).siblings('input').each(function( index ) {
        if (!$(this).val()) {
            $(this).addClass('error')
            has_error = 1
        }
    })
    if (has_error) { return; }

    var params = []
    $(this).siblings('.b-input').each(function (index, element) {
        params.push($(element).val())
    })
    $.get( '/api/command', {
        device_code:  $(this).data('device_code'),
        command_code: $(this).data('command_code'),
        params:       params
    })
    .done(function( data ) {
        if (data.success) {
            $('#selection_total').html(data.value.selection_total)
            $('#max_weight').html(data.value.max_weight)
            $('#min_weight').html(data.value.min_weight)
        }
    })
    .fail(function() {

    })
})

$(document).on('click', '#new_batch', function(e) {
    e.preventDefault()
    $('#new_batch_title').show()
    $('#new_batch_start_count').show()
    $('#new_batch_save').show()
    $('#new_batch').hide()
})

$(document).on('click', '#new_batch_save', function(e) {
    e.preventDefault()
    var params = {}
    params['title'] = $('#new_batch_title').val()
    params['start_count'] = $('#new_batch_start_count').val()
    $.post('/api/create_batch', params)
    .done(function( data ) {
        if (data.success) {
            $('#new_batch_title').val('')
            $('#new_batch_title').hide()
            $('#new_batch_start_count').val('')
            $('#new_batch_start_count').hide()
            $('#new_batch_save').hide()
            $('#batch_title').html(data.value.title)
            $('#batch_start_count').html(data.value.start_count)
            $('#batch_current_count').html(data.value.current_count)
            $('#batch_start_time').html(data.value.start_date)
            $('#batch_days_from_start').html(data.value.days_from_start)
            // HACK
            $('#deaths_total').html('0')
            $('#deaths_last_7_days').html('0')
            $('#new_batch').show()
        }
    })
    .fail(function() {

    })
})

$(document).on('click', '#fix_current_count', function(e) {
    e.preventDefault()
    $('#fix_current_count_title').show()
    $('#fix_current_count_value').show()
    $('#fix_current_count_save').show()
    $('#fix_current_count').hide()
})

$(document).on('click', '#fix_current_count_save', function(e) {
    e.preventDefault()
    var params = {}
    params['title'] = $('#fix_current_count_title').val()
    params['current_count'] = $('#fix_current_count_value').val()
    $.post('/api/fix_current_count', params)
    .done(function( data ) {
        if (data.success) {
            $('#fix_current_count_title').val('')
            $('#fix_current_count_title').hide()
            $('#fix_current_count_value').val('')
            $('#fix_current_count_value').hide()
            $('#fix_current_count_save').hide()
            $('#batch_current_count').html(data.value.current_count)
            $('#fix_current_count').show()
        }
    })
    .fail(function() {

    })
})

$(document).on('click', '#pig_death', function(e) {
    e.preventDefault()
    $('#pig_death_title').show()
    $('#pig_death_save').show()
    $('#pig_death').hide()
})

$(document).on('click', '#pig_death_save', function(e) {
    e.preventDefault()
    var params = {}
    params['title'] = $('#pig_death_title').val()
    $.post('/api/pig_death', params)
    .done(function( data ) {
        if (data.success) {
            $('#pig_death_title').val('')
            $('#pig_death_title').hide()
            $('#pig_death_save').hide()
            $('#batch_current_count').html(data.value.current_count)
            $('#deaths_total').html(data.value.deaths_total)
            $('#deaths_last_7_days').html(data.value.deaths_last_7_days)
            $('#pig_death').show()
        }
    })
    .fail(function() {

    })
})
