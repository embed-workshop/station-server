var ws_interval
    , ws

var poll = function() {

    if (ws_interval) {
        clearInterval(ws_interval)
    }

    ws = new WebSocket('ws://' + window.location.host + '/ws_polling/weighing')

    ws.onopen = function() {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        ws_interval = setInterval(function(){
            try {
                ws.send('ping')
            }
            catch(e) {
            }
        }, 200)
    };

    ws.onclose = function(event) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
        setTimeout( poll, 1000);
    };

    ws.onmessage = function(event) {
        var data = JSON.parse(event.data)

        if ($('.value').html() != data.value) {
            $('.value').html(data.value)
        }
    };

    ws.onerror = function(error) {
        if (ws_interval) {
            clearInterval(ws_interval)
        }
    };

}

$(document).ready(function(){

    poll()

})

