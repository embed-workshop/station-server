var station_params = ['selection_total', 'max_weight', 'min_weight', ]
var change_station_params = function(index, value) {
    var item = value
    $(document).on('click', '#' + item, function(e) {
        e.preventDefault()
        $('#' + item + '_value').show()
        $('#' + item + '_save').show()
        $('#' + item).hide()
    })

    $(document).on('click', '#' + item + '_save', function(e) {
        e.preventDefault()
        var params = {}
        params[item] = $('#' + item + '_value').val()
        $.post('/api/station_state', params)
        .done(function( data ) {
            if (data.success) {
                $('#' + item + '_value').hide()
                $('#' + item + '_save').hide()
                $('#' + item).html(data.value[item])
                $('#' + item).show()
            }
        })
        .fail(function() {

        })
    })
}

$.each(station_params, change_station_params)

$(document).keyup(function(e) {
    if (e.keyCode === 27) {
        for (var i = 0; i < station_params.length; i++) {
            var item = station_params[i]
            $('#' + item + '_value').hide()
            $('#' + item + '_save').hide()
            $('#' + item ).show()
        }
        $('#new_batch').show()
        $('#new_batch_title').hide()
        $('#new_batch_start_count').hide()
        $('#new_batch_save').hide()
        $('#fix_current_count').show()
        $('#fix_current_count_title').hide()
        $('#fix_current_count_value').hide()
        $('#fix_current_count_save').hide()
        $('#pig_death').show()
        $('#pig_death_title').hide()
        $('#pig_death_save').hide()
    }
});

function show_info_popup(text) {
    $('#GlobalMessageType1').html('<div class="MessageType1"><p>' + text + '</p></div>')
    setTimeout(function(){ $('.MessageType1').fadeOut(1000); }, 5000);
}
function show_error_popup(text) {
    $('#GlobalMessageType1').html('<div class="MessageType3"><p>' + text + '</p></div>')
    setTimeout(function(){ $('.MessageType3').fadeOut(1000); }, 10000);
}

$(document).ready(function() {

    if ( info_message ) {
        show_info_popup( info_message )
    }

    if ( error_message ) {
        show_error_popup( error_message )
    }

})
