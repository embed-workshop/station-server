$(document).on('click', 'a.command_button', function(e) {

    e.preventDefault()

    var params = []
    $(this).siblings('.command_param').each(function (index, element) {
        params.push($(element).val())
    })

    $.get( '/api/command', {
        device_code:  $(this).data('device_code'),
        command_code: $(this).data('command_code'),
        params:       params
    })
    .done(function( data ) {
        if (data.success && data.setting)
        $('#setting_' + data.setting).html(data.value)
    })
    .fail(function() {

    })
});