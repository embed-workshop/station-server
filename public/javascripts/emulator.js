$(document).on('click', 'a.emulate', function(e) {

    e.preventDefault()
    var bytes = []
    $(this).siblings('input').each(function(){ bytes.push($(this).val()) })
    $.get( '/api/emulate', {
        bytes: bytes
    })
    .done(function( data ) {
        if (data.success) {
            console.log('success')
        }
        else {
            console.log('error')
        }
    })
    .fail(function() {
        console.log('error')
    })
});