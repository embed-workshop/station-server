# Station Server #

### Installing nodejs ###

Recommended version - LTS v6.10.2.
Follow instructions [here](https://nodejs.org/en/download/package-manager/).

Ubuntu 16.04 version from [here](https://askubuntu.com/questions/786272/why-does-installing-node-6-x-on-ubuntu-16-04-actually-install-node-4-2-6):

1. Create a new file: /etc/apt/sources.list.d/nodesource.list and this inside it:  
   ```  
   deb https://deb.nodesource.com/node_6.x xenial main  
   deb-src https://deb.nodesource.com/node_6.x xenial main
   ```

2. Download the GPG Signing Key from Nodesource for the repository.
   ```
   curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
   ```
3. Manually run sudo apt-get update.
4. Check apt-cache policy nodejs output.
5. Install the nodejs binary.
   ```
   sudo apt-get install nodejs
   ```
### Installing Station Server itself ###

1. execute commands
   ```
   cd /home/pi/  
   git clone git@bitbucket.org:embedlab/station_server.git
   cd /home/pi/station_server
   ./install
   ```

### Installing postgres(move to install script) ###
[ubuntu example](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)
```
sudo apt-get install postgresql-9.1
sudo service postgresql start
sudo -i -u pi
```
ответить pi и y
```
createdb pi
sudo -u pi psql
```
выполнить
ALTER USER pi WITH PASSWORD 'raspberry';
\q
```
exit
node models/init.js
```

### Configuring systemd ###

from [here](https://www.axllent.org/docs/view/nodejs-service-with-systemd/)  

1. Set up the service
   Create a file in /etc/systemd/system/station_server.service:  

   [Unit]  
   Description=StationServer  
   [Service]  
   ExecStart=/usr/bin/npm start  
   WorkingDirectory=/home/pi/station_server  
   Restart=always  
   RestartSec=10  
   StandardOutput=syslog  
   StandardError=syslog  
   SyslogIdentifier=StationServer  
   Environment=NODE_ENV=production PORT=3001  
   [Install]  
   WantedBy=multi-user.target  

2. Enable the service
   ```
   systemctl enable station_server.service
   ```
3. Start the service
   ```
   systemctl start station_server.service
   ```
4. Verify it is running
   ```
   systemctl status station_server.service
   ```
# Установка на Raspberry Pi Compute Module #
## Установка системы на модуль ##
1. ...
1. ...
## Подготовка системы к работе ##
## Подготовка железа ##
### UART ###
1. ...
1. ...
### RTC ###
1. https://cdn-learn.adafruit.com/downloads/pdf/adding-a-real-time-clock-to-raspberry-pi.pdf
1. ...
### STM32 bootloader ###
1. sudo apt-get install stm32flash 
1. stm32flash /dev/ttyAMA0 -w file_name.hex -v -R -i 9,-0,0:-9,-0,0   (https://sourceforge.net/p/stm32flash/wiki/Home/)

### Установка NodeJS и сервера станции ###
1. cd /home/pi/quest
1. wget https://nodejs.org/dist/v6.10.2/node-v6.10.2-linux-armv7l.tar.xz
1. tar -xvf node-v6.10.2-linux-armv7l.tar.xz
1. cd node-v6.10.2-linux-armv7l
1. sudo cp -R * /usr/local/
1. git clone https://bitbucket.org/embedlab/station_server
1. cd ./station_server
1. npm install
1. chmod -R 777 *
1. npm start
### Настройка GUI и браузера для локального дисплея ###
1. sudo apt-get install --no-install-recommends xserver-xorg
1. sudo apt-get install --no-install-recommends 
1. sudo apt-get install raspberrypi-ui-
1. sudo apt-get install lightdm
1. sudo apt-get install epiphany-browser
1. nano /home/pi/station_server/start_display
1. В файл записать: epiphany-browser -a --profile ~/.config http://localhost:3000/display
1. startx ./station_server/start_display