const QueryFile = require('pg-promise').QueryFile;
const path      = require('path');

function sql(file) {
    const fullPath = path.join(__dirname, file);
    return new QueryFile(fullPath, {minify: true});
}

module.exports = {

    events: {
        select_from:        sql( 'sql/events/select_from.sql' ),
        find:               sql( 'sql/events/find.sql' ),
        delete:             sql( 'sql/events/delete.sql' ),
        deaths_total:       sql( 'sql/events/deaths_total.sql' ),
        deaths_last_7_days: sql( 'sql/events/deaths_last_7_days.sql' ),
        select_by_date:     sql( 'sql/events/select_by_date.sql' ),
        select_errors:      sql( 'sql/events/select_errors.sql' ),
        stats:              sql( 'sql/events/stats.sql' ),
        create:             sql( 'sql/events/create.sql' ),
    },

    batches: {
        select_from:        sql( 'sql/batches/select_from.sql' ),
        select_all:         sql( 'sql/batches/select_all.sql' ),
        find_last:          sql( 'sql/batches/find_last.sql' ),
        find_last_2:        sql( 'sql/batches/find_last_2.sql' ),
        find_current:       sql( 'sql/batches/find_current.sql' ),
        inc_current_count:  sql( 'sql/batches/inc_current_count.sql' ),
        dec_current_count:  sql( 'sql/batches/dec_current_count.sql' ),
        set_current_count:  sql( 'sql/batches/set_current_count.sql' ),
        end_current:        sql( 'sql/batches/end_current.sql' ),
        create:             sql( 'sql/batches/create.sql' ),
    },

    settings: {
        select_all:         sql( 'sql/settings/select_all.sql' ),
        update:             sql( 'sql/settings/update.sql' ),
    },
};