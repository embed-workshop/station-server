var jsonfile = require('jsonfile')
var fs       = require('fs')
var log      = require('./log.js')

exports.read = function(){
    var config_file = __dirname + '/config.json'
    var config = {}
    if (!fs.existsSync(config_file)) {
       config_file = __dirname + '/config.default.json'
    }

    try {
        config = jsonfile.readFileSync(config_file, {throws: true})
    }
    catch(e) {
        log.error('config file read error')
    }
    return config
}