var options = {
};

var pgp = require('pg-promise')(options);
var cn = {
    host: 'localhost', 
    port: 5432,
    database: 'pi',
    user: 'pi',
    password: 'raspberry'
};

var db = pgp(cn);


db.query(`CREATE TABLE batches (
    id                     SERIAL PRIMARY KEY,
    title                  VARCHAR(1024)               DEFAULT '',
    start_time             TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL,
    end_time               TIMESTAMP WITHOUT TIME ZONE,
    start_count            INT,
    current_count          INT,
    status                 INT DEFAULT 1
)`)

db.query(`
CREATE TABLE settings (
    id                     SERIAL PRIMARY KEY,
    code                   INT,
    title                  VARCHAR(1024),
    value_int              INT,
    value_text             TEXT
)
`
)
.then(() => {
    db.query(`
INSERT INTO settings (code, title, value_int) VALUES
    ( 4,               'порог срабатывания весов', 1000),
    ( 5,                             'гистерезис', 1000),
    ( 6,                  'время удержания массы', 1000),
    ( 7,    'время аварии при нестабильной массе', 1000),
    ( 8,             'время до начала стимуляции', 1000),
    ( 9,           'продолжительность стимуляции', 1000),
    (10,          'количество попыток стимуляции', 1000),
    (11, 'время между двумя попытками стимуляции', 1000)
`
    )
});

db.query(`CREATE TABLE event_types (
    id                     SERIAL PRIMARY KEY,
    name                   VARCHAR(1024)               DEFAULT '',
    title                  VARCHAR(1024)               DEFAULT ''
)`)
.then(() => {
    db.query(`INSERT INTO event_types (name, title) VALUES
        ('station_error',        'Ошибка устройства \"Логика станции\"'),
        ('weights_error',        'Ошибка устройства \"Показания весов\"'),
        ('entr_door_man_error',  'Ошибка устройства \"Управление входной дверью\"'),
        ('life_door_man_error',  'Ошибка устройства \"Управление дверью в жизнь\"'),
        ('death_door_man_error', 'Ошибка устройства \"Управление дверью на убой\"'),
        ('stimulation_error',    'Ошибка устройства \"Управление стимуляцией\"'),
        ('entr_door_st_error',   'Ошибка устройства \"Состояние входной двери\"'),
        ('life_door_st_error',   'Ошибка устройства \"Состояние двери в жизнь\"'),
        ('death_door_st_error',  'Ошибка устройства \"Состояние двери на убой\"'),
        ('weighing',             'Взвешивание'),
        ('death',                'Смерть свиньи'),
        ('correction',           'Коррекция количества свиней'),
        ('success_weighing',     'Отбор')
    `)
    .then(() => {
        db.query(`CREATE TABLE events (
            id                     SERIAL PRIMARY KEY,
            type_id                INT REFERENCES event_types(id),
            value_int              INT,
            value_text             TEXT,
            datetime               TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW() NOT NULL,
            status                 INT DEFAULT 1
        )`)
        .then(() => {
            db.query('CREATE INDEX type_id_idx ON events (type_id)');

            db.query('CREATE INDEX datetime_idx ON events (datetime)');

            db.query('CREATE INDEX status_idx ON events (status)');

            console.log('done, press Ctrl-C')
        });
    });
});