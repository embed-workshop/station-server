var http     = require('http')

var log      = require('./log.js')
var config   = require('./config.js')
var station  = require('./station.js')
var devices  = require('./devices.js')

var main_server_url = config.read().main_server.url,
    cpuid           = station.params.uid

exports.poll_url        = `http://${main_server_url}/stations/connect?cpuid=${cpuid}`

exports.poll = function() {

    var state = devices.list[0].value + 0,
        poll_url = exports.poll_url + `&state=${state}`

    http.get( poll_url, res => {

        if (res.statusCode !== 200) {
            log.error(`connect request failed`);
            setTimeout(exports.poll, 5000)
            return;
        }

        var rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
            try {
                const parsedData = JSON.parse(rawData);
                log.info(parsedData);
                if (parsedData.success) {
                    station.write({
                        group: parsedData.station_group,
                        name:  parsedData.station_name
                    })
                    setTimeout(exports.poll, 1000)
                }
                else if (parsedData.connected) {
                    setTimeout(exports.poll, 60*1000)
                }
                else {
                    setTimeout(exports.poll, 5000)
                }
            } catch (e) {
                log.error(e.message);
                setTimeout(exports.poll, 5000)
            }
        });
    }).on('error', (e) => {
        log.error(`Got error: ${e.message}`);
        setTimeout(exports.poll, 5000)
    });
}